import fs from 'fs';
import BpmnModdle from 'bpmn-moddle';
import {BPMNProcess} from './semantics/bpmn-semantics';
import {createStore} from 'redux';
import {BPMNUtil} from './util/bpmn-util';
import https from 'http';
import cors from 'cors';
import express from 'express';
import Server from 'socket.io';

// var pkey = fs.readFileSync('./server.key');
// var pcert = fs.readFileSync('./server.crt')
//
// var options = {
//     key: pkey,
//     cert: pcert
// };

var app = express();
app.use(cors());
var httpsServer = https.createServer(app);
var server = Server.listen(https);

var xml;
var bePackage = require('./resources/be.json');

var currentBusinessSchema;
var store;
var proc;


console.log('starting server at: ');
console.log(process.env.PORT || 8000);
//var server = new Server(9090);
httpsServer.listen(process.env.PORT || 8000);

app.get('/', function (req, res) {
  res.send('Hello World!');
});


function openDiagram(moddle, diagramXML, socket, businessSchema, flows = null, elements = null) {
  moddle.fromXML(diagramXML, (err, definitions) => {

    // Preprocessing of XML file
    currentBusinessSchema = businessSchema;
    proc = new BPMNProcess();
    // If diagram contains pools and lanes, find the process
    var procDef = proc.getProcess(definitions.rootElements);
    var seqFlowEdges = procDef.flowElements.filter(function(elem){
      return proc.isSequenceFlow(elem);
    });

    // Get all the sub processes
    var subProcesses = proc.collectSubProcesses(procDef);

    // Include the sequence -flows in the subprocesses in the edge set as well
    for(let subProc of subProcesses) {
      for(let subEl of subProc.flowElements) {
        if(proc.isSequenceFlow(subEl)) {
          seqFlowEdges.push(subEl);
        }
      }
    }

    for (let edge of seqFlowEdges)
      proc.addFlowEdge(edge);
    proc.pack();

    const reductor = (state = {allElements: proc.elements, executed: [],
                                enabled: proc.sources, selectable: [],
                                tokens: [], blocked: [], deselectable: [], started: false},
                                action) => {
      switch (action.type) {
        case 'FIRE_ELEMENT':
          if (state.enabled.length > 0) {
            const newState = proc.fireElement(state);
            return newState;
          }
          return state;
        case 'SELECT_ELEMENT':
          if(state.selectable.length > 0 || state.deselectable.length > 0) {
            var elem = proc.getElement(state, action.element);

            const newState = proc.selectElement(state, action.element, action.selection);
            return newState;
          }
          return state;
        case 'PROCEED_ELEMENT':
          var elem = proc.getElement(state, action.element.id);
          if(BPMNUtil.containsElement(state.enabled, elem)) {
            const newState = proc.handleElementExecution(state, elem);
            return newState;
          }
          else if(proc.isElementSelectable(state, elem.id)) {
            const newState = proc.selectElement(state, elem.id, 'selectable');
            return newState;
          }
          else if(BPMNUtil.containsElement(state.deselectable, elem)) {
            const newState = proc.selectElement(state, elem.id, 'deselectable');
            return newState;
          }
          return state;
        case 'DISABLE_ELEMENT':
          var elem = proc.getElement(state, action.element);
          var newState = proc.deselectElement(state, elem);
          return newState;
        case 'SIMULATION_STARTED':
          return {allElements: state.allElements.slice(), executed: state.executed.slice(), enabled: state.enabled.slice(),
                  selectable: state.selectable.slice(), tokens: state.tokens.slice(), blocked: state.blocked.slice(),
                  deselectable: state.deselectable.slice(), started: true};
        case 'UPDATE_ELEMENT':
          var element = proc.getElement(store.getState(), action.elementId);
          if(element) {
            if(proc.isUserTask(element)) {
              var schema = proc.getExtension(element, 'be:BusinessTask');
              schema.activationSchema = action.extension;
            }
            else if(proc.isMessageEvent(element)) {
              var messageExt = proc.getExtension(element, 'be:MessageEvent');
              messageExt.message = action.extension;
            }
            else if(proc.isScriptTask(element)) {
              var scriptExt = proc.getExtension(element, 'be:ScriptTask');
              scriptExt.script = action.extension;
            }
          }
          return state;
        default:
          return state;
      }
    };

    //const store = createStore(reductor);
    store = createStore(reductor);

    // initialize the extensions for all elements
    for(let el of store.getState().allElements) {
      el.extensionElements = moddle.create('bpmn:ExtensionElements');
      el.extensionElements.get('values');

      if(proc.isUserTask(el)) {
        var schema = moddle.create('be:BusinessTask');
        el.extensionElements.get('values').push(schema);
        schema.activationSchema = "{}";
      }
      else if(proc.isMessageEvent(el)) {
        var schema = moddle.create('be:MessageEvent');
        el.extensionElements.get('values').push(schema);
        schema.message = "{}";
      }
      else if(proc.isScriptTask(el)) {
        var schema = moddle.create('be:ScriptTask');
        el.extensionElements.get('values').push(schema);
        schema.script = "{}";
      }

      if(elements) {
        var receivedElement = elements.filter(function(v) {
            return v.id === el.id; // Filter out the appropriate one
        })[0];

        if(receivedElement) {
          if(proc.isUserTask(el)) {
            var activationSchemaExt = proc.getExtension(el, 'be:BusinessTask');
            var receivedActivationSchemaExt = proc.getExtension(receivedElement, 'be:BusinessTask');
            activationSchemaExt.activationSchema = receivedActivationSchemaExt.activationSchema;
          }
          else if(proc.isMessageEvent(el)) {
            var messageExt = proc.getExtension(el, 'be:MessageEvent');
            var receivedMessageExt = proc.getExtension(receivedElement, 'be:MessageEvent');
            messageExt.message = receivedMessageExt.message;
          }
          else if(proc.isScriptTask(el)) {
            var scriptExt = proc.getExtension(el, 'be:ScriptTask');
            var receivedScriptExt = proc.getExtension(receivedElement, 'be:ScriptTask');
            scriptExt.script = receivedScriptExt.script;
          }
        }
      }
    }

    // initialize the extensions for all flow sequenceFlows
    for(let flow_map of proc.sequenceFlows) {
      var flow = flow_map[1];
      flow.extensionElements = moddle.create('bpmn:ExtensionElements');
      flow.extensionElements.get('values');

      if(proc.isSequenceFlow(flow)) {
        var schema = moddle.create('be:FlowCondition');
        flow.extensionElements.get('values').push(schema);
        schema.condition = "{}";
      }

      // If the elements were received from uploading diagrams, copy the extensions
      if(flows) {
        var receivedElement = flows.filter(function(v) {
            return v.id === flow.id; // Filter out the appropriate one
        })[0];

        if(receivedElement) {
          var conditionExt = proc.getExtension(flow, 'be:FlowCondition');
          var receivedConditionExt = proc.getExtension(receivedElement, 'be:FlowCondition');
          conditionExt.condition = receivedConditionExt.condition;
        }
      }
    }

    // Broadcast does not send the data to the source client
    socket.broadcast.emit('model', {data: xml, state: store.getState(), businessObject: currentBusinessSchema, businessObjectData: proc.businessObject,
                          flows: JSON.stringify(proc.sequenceFlows)});
    socket.emit('model', {data: xml, state: store.getState(), businessObject: currentBusinessSchema, businessObjectData: proc.businessObject,
                          flows: JSON.stringify(proc.sequenceFlows)});
  });
};

function acceptConnections(server) {
  server.on('connection', socket => {
    console.log('new client');
    socket.initialization = true;
    socket.on('diagramLoaded', data => {
      xml = data;
      openDiagram(moddle, xml, socket, "{}");
    });
    socket.on('diagramUploaded', data => {
      xml = data.diagram;
      openDiagram(moddle, xml, socket, data.businessData, data.flows, data.elements);
    });
    socket.on('diagram', () => {
      if(socket.initialization) {
        socket.on('action', data => {
            store.dispatch(data);
        });
        socket.on('dataObject', data => {
          currentBusinessSchema = data.schema;
          socket.broadcast.emit('dataObject', data);
        });
        socket.on('businessObject', data => {
          proc.businessObject = data.schema;
          socket.broadcast.emit('businessObject', data);
        });
        socket.on('defineCondition', data => {
          var flow = proc.sequenceFlows.get(data.elementId);
          if(flow) {
            var conditionExt = proc.getExtension(flow, 'be:FlowCondition');
            conditionExt.condition = data.extension;
          }
          socket.broadcast.emit('defineCondition', flow);
        });
        socket.on('askForData', () => {
          var flowKeys = proc.sequenceFlows.keys();
          var flowArray = [];
          for(let fk of flowKeys) {
            flowArray.push(proc.sequenceFlows.get(fk));
          }
          socket.emit('sendData', {elements: store.getState().allElements,
                                    flows: flowArray,
                                    businessData: currentBusinessSchema});
        });
        socket.initialization = false;
      }
      store.subscribe(() => {
          socket.emit('state', {state: store.getState()});
      });
    });
    // If a diagram is already loaded
    if(xml) {
      socket.emit('model', {data: xml, state: store.getState(), businessObject: currentBusinessSchema, businessObjectData: proc.businessObject,
                            flows: JSON.stringify(proc.sequenceFlows)});
    }
  });
}

var moddle = new BpmnModdle([bePackage]);
acceptConnections(server);
